import glob
import os
import markdown
from datetime import datetime
import util
import uuid
import subprocess
import sys

def run(params, variables, log):
	dir = params[0]
	tpl = params[1]
	blog_dir = params[2]
	
	bar = ''
	for f in glob.glob(os.path.join(dir, '*')):
		fh = open(f, 'r')
		foo = ''
		post_vars = {}
		for line in fh.readlines():
			if line.startswith('%'):
				k = line[1:].split('=')[0].strip()
				v = line[1:].split('=')[1].strip()
				post_vars[k] = v
				continue
			foo += line

		fh.close()
		post_vars['post_content'] = markdown.markdown(foo.strip(), extensions=['extra', 'codehilite'], output_format='html5')
		post_vars['post_url'] = '/blog/p/' + post_vars['post_title'].replace(' ', '-').lower() + '.html'
		post_vars['post_date'] = datetime.strptime(post_vars['post_date'], '%m/%d/%Y').strftime('%d %B %Y')
		post_vars['post_time'] = datetime.strptime(post_vars['post_time'], '%H%M').strftime('%I:%M %p')
		bar += util.get_template(tpl, post_vars)
		
	blog_page_tpls = params[3].split(',')
	
	for f in glob.glob(os.path.join(dir, '*')):
		bar1 = ''
		fh = open(f, 'r')
		foo = ''
		post_vars = {}
		for line in fh.readlines():
			if line.startswith('%'):
				k = line[1:].split('=')[0].strip()
				v = line[1:].split('=')[1].strip()
				post_vars[k] = v
				continue
			foo += line

		fh.close()
		post_vars['post_content'] = markdown.markdown(foo.strip(), extensions=['extra', 'codehilite'], output_format='html5')
		post_vars['post_url'] = '/blog/p/' + post_vars['post_title'].replace(' ', '-').lower() + '.html'
		post_vars['post_date'] = datetime.strptime(post_vars['post_date'], '%m/%d/%Y').strftime('%d %B %Y')
		post_vars['post_time'] = datetime.strptime(post_vars['post_time'], '%H%M').strftime('%I:%M %p')
		for v in variables:
			post_vars[v] = variables[v]
			
		fname = str(uuid.uuid4())
		fh = open(fname,"w")
		fh.write('['+os.path.join(blog_dir, post_vars['post_title'].replace(' ', '-').lower() + '.html')+']\n')
		for item in post_vars:
			if(item != 'blog_index'):
				fh.write('$'+item+'='+post_vars[item].replace('\n','{NEWLINE}')+'\n')
		for a, item in enumerate(blog_page_tpls):
			fh.write(str(a) + '=' + item + '\n')
		fh.close()

		subprocess.call([sys.executable,'quicksite.py','-i',fname])
		
		os.unlink(fname)

	return bar
