import os, glob, StringIO, sys, contextlib
from string import Template
from ConfigParser import ConfigParser
from collections import OrderedDict

@contextlib.contextmanager
def stdoutIO(stdout=None):
	old = sys.stdout
	if stdout is None:
		stdout = StringIO.StringIO()
	sys.stdout = stdout
	yield stdout
	sys.stdout = old

def makedir(newdir):
	if os.path.isdir(newdir):
		pass
	elif os.path.isfile(newdir):
		#TODO
		pass
	else:
		head, tail = os.path.split(newdir)
		if head and not os.path.isdir(head):
			makedir(head)
		if tail:
			os.mkdir(newdir)

def get_template(tpl,d1):

	foo = ''
	while 1:
		try:
			foo = Template(open(tpl,'r').read()).substitute(d1)
			break
		except KeyError as r:
			d1[str(r)[1:-1]] = ''
	
	while '<%' in foo and '%>' in foo:
		b = foo.find('<%')
		e = foo[b+2:].find('%>')
		with stdoutIO() as con:
			exec(foo[b+2:b+e+2].strip())
		foo = foo.replace(foo[b:b+e+4],con.getvalue().rstrip())
		
	return foo.replace('{NEWLINE}','\n')

def parse_calls(foo,d,mod,log):

	for section in foo:

		log.subtitle('Processing: %s'%section)
			
		d1 = d[section]

		gvars = d[';globals']
		for item1 in gvars.keys():
			if item1 in foo[section].values():
				for item2 in gvars[item1].keys():
					d1[item2] = gvars[item1][item2]

		for i in d1:

			if d1[i].startswith('^'):
				f1 = d1[i].find('^')+1
				l1 = d1[i].find('(')
				fx = d1[i][f1:l1]
				p = d1[i][l1+1:-1]
				
				params = [x.strip() for x in p.split('|')]
				
				if fx in mod:
					_quick = __import__('modules.'+fx, fromlist=[fx])
					d1[i] = eval('_quick.run(params, d1, log)')
					d1[i] = d1[i]
				
				log.status('$%s = %s' % (i, fx))
				
				for n, p1 in enumerate(params):
					log.status('\tparams[%i] = %s'% (n,p1))
			else:
				log.status('$%s = %s' % (i, d1[i]))
		
		if os.path.basename(section).split('.')[-1] == 'html' or os.path.basename(section).split('.')[-1] == 'htm': 
			f = open(section,'w')
		else:
			makedir(section)
			f = open(os.path.join(section,'index.html'),'w')

		for n in foo[section].keys():
			f.write(get_template(foo[section][n],d1))
		log.status('Wrote %s' % f.name)
			
		f.close()