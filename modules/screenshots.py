from PIL import Image
from collections import OrderedDict
import json, os

def run(params, variables, log):

	d2 = json.loads(open(params[0],'r').read(), object_pairs_hook=OrderedDict)
	foobar = []

	for c in d2:
		alt = c
		
		if not d2[c].endswith('!'):

			j = d2[c].rfind('.')
			
			url = d2[c]
			thumb = d2[c][:j] + '_thumb.' + d2[c][j+1:]

			im = Image.open(os.path.join(params[1],url[1:]))
			if im.mode != 'RGB': im = im.convert('RGB')
			
			w,h = im.size
			x = (128*w)/h
			
			im = im.resize((x,128), Image.ANTIALIAS)
			im.save(os.path.join(params[1],thumb[1:]),'JPEG')
			
			foobar.append('<li class="thumbnail"><a href="'+url+'"><img alt="'+alt+'" src="'+thumb+'"></a></li>')
		
		else:
			url = d2[c][:-1]
			foobar.append('<li class="thumbnail"><img alt="'+alt+'" src="'+url+'"></li>')

	return  '<ul id="screenshots-ul">' + ' '.join(foobar) + '</ul>'